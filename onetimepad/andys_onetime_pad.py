import random

FIRST_CH = 32
LAST_CH = 126
CH_RANGE = LAST_CH - FIRST_CH


def gen_pad(n):
    """
    Generate a one-time pad of values within the range of printable ASCII
    characters from space (32) to ~ (126).

      >>> pad = gen_pad(10)
      >>> len(pad)
      10
      >>> all([isinstance(i, int) for i in pad])
      True
      >>> all([i <= CH_RANGE for i in pad])
      True
    """
    return [random.randint(0, CH_RANGE) for i in range(n)]


def encrypt(message, pad):
    """
    Encrypt a message using a one-time pad. Return helpful error messages if:

    1. The pad is too short
    2. The message contains characters not between ASCII space (32) and ~ (126)

      >>> message = "This is only a test, do not be alarmed!"
      >>> pad = [1, 2, 3, 10, 20, 30, 0, 126, 5, 10, 15, 20, 25, 30, 1, 99, 8]
      >>> encrypt(message, pad)
      Pad length too short.
      >>> message = "This is\\na test!"
      >>> encrypt(message, pad)
      Message must have only ASCII characters between 32 and 126.
      >>> message = "This is a test!"
      >>> encrypt(message, pad)
      'Ujl}4)s@f*%y.4"'
    """
    try:
        assert len(pad) >= len(message), "Pad length too short."
    except AssertionError as msg:
        print(msg)
        return

    try:
        err_msg = "Message must have only ASCII characters between 32 and 126."
        assert all([32 <= ord(ch) <= 126 for ch in message]), err_msg
    except AssertionError as msg:
        print(msg)
        return

    encrypted = ""

    for i, ch in enumerate(message):
        encrypted += chr((ord(ch) - FIRST_CH + pad[i]) % CH_RANGE + FIRST_CH)

    return encrypted


def decrypt(ciphertext, pad):
    """
    This should decrypt the original message using the same one-time pad and
    return the original message. Same checks for pad length and message
    characters inforced.

      >>> code = 'Ujl}4)s@f*%y.4"'
      >>> pad = [1, 2, 3, 10, 20, 30, 0, 126, 5, 10, 15, 20, 25, 30]
    """
    try:
        assert len(pad) >= len(ciphertext), "Pad length too short."
    except AssertionError as msg:
        print(msg)
        return

    try:
        err_msg = "Coded message must have only ASCII characters between" "32 and 126."
        assert all([32 <= ord(ch) <= 126 for ch in ciphertext]), err_msg
    except AssertionError as msg:
        print(msg)
        return

    decrypted = ""

    for i, ch in enumerate(ciphertext):
        decrypted += chr((ord(ch) - FIRST_CH - pad[i]) % CH_RANGE + FIRST_CH)

    return decrypted


if __name__ == "__main__":
    import doctest

    doctest.testmod()
