from gasp import *


def draw_face(x, y):
    # Face
    Circle((x, y), 40)
    # Nose
    Line((x-10, y-10), (x, y+10))
    Line((x-10, y-10), (x+10, y-10))
    # Eyes
    Circle((x-15, y+10), 5)
    Circle((x+15, y+10), 5)
    # Mouth
    Arc((x, y), 30, 225, 315)


begin_graphics(1200, 800)

for y in range(40, 761, 80):
    for x in range(40, 1161, 80):
        draw_face(x, y)

update_when('key_pressed')
end_graphics()
