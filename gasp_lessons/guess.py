from random import randint

num = randint(1, 1000)

guess = int(input("I'm thinking of a number between 1 and 1000. What is it? "))
guesses = 1

while guess != num:
    if guess > num:
        guess = int(input("Too high, please try again. "))
    else:
        guess = int(input("Too low, please try again. "))
    guesses += 1

print(f"Congratulations, you got it in {guesses} guesses!")
if guesses < 11:
    print("You rock!")
elif guesses < 20:
    print("Not bad, but you could do better.")
else:
    print("Time to learn your math!")
