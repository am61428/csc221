import random
from gasp.utils import read_number 


def add(a, b):
    return a, b, a + b, 'plus'


def sub(a, b):
    return a + b, a, b, 'minus'


def mul(a, b):
    return a, b, a * b, 'times'


def div(a, b):
    return a * b, a, b, 'divided by'


opperators = [add, mul, sub, div]


def quiz(opprs, numqs=10, minv=0, maxv=10):
    right = 0
    for q in range(numqs):
        arg1 = random.randint(minv, maxv)
        arg2 = random.randint(minv, maxv)
        oppr = random.choice(opprs)

        op1, op2, correct, oppr_name = oppr(arg1, arg2)
        answer = read_number(f"What's {op1} {oppr_name} {op2}? ")

        if answer == correct:
            print("That's right -- well done.")
            right += 1
        else:
            print(f"No, I'm afraid the answer is {correct}.")

    print(f'\nI asked you {numqs} questions. You got {right} of them right.')
    print('Well done!')


quiz(opperators)
