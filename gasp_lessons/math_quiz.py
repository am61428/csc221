from random import randint
from gasp.utils import read_number 


def quiz(numqs=10, op1=(0, 10), op2=(0, 10)):
    right = 0
    for q in range(numqs):
        operand1 = randint(op1[0], op1[1])
        operand2 = randint(op2[0], op2[1])
        answer = read_number(f"What's {operand1} times {operand2}? ")
        correct = operand1 * operand2
        if answer == correct:
            print("That's right -- well done.")
            right += 1
        else:
            print(f"No, I'm afraid the answer is {correct}.")

    print(f'\nI asked you {numqs} questions. You got {right} of them right.')
    print('Well done!')


quiz(4, (0, 10), (1, 13))
