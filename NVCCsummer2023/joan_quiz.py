from random import randint

num1 = randint(1, 10)
num2 = randint(1, 10)

question = "What is " + str(num1) + " times " + str(num2) + "? "
answer = int(input(question))

if answer == num1 * num2:
    print("That's right. Well done!")
else:
    print("No, I'm afraid the answer is " + str(num1 * num2) + ".")
