# Boolean Search 

During the *Lunchtime Library Lecture* on June 28th, the presenter shared an
example boolean search, which was something like this:

    "artifical intelligence" and ("cheating" or "academic dishonesty")

Learning is enhanced by making connections between different areas of study.
Not wanting to miss an opportunity to do this, I jotted down the search
expression when I got home (you all have to help me remember what she put
for the second half of the ``or`` expression).

We can use this as an entrée into an important topic in computer programming:
**boolean expressions**.


## Resources

* [What Is a Boolean Search?](https://www.lifewire.com/what-does-boolean-search-3481475)
