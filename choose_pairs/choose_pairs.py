#!/usr/bin/env python3
import os
import random
import sys
import time


def main(names_file):
    with open(names_file) as f:
        students = [student.strip() for student in f.readlines()]

    print('\nPairs for the GASP Python Course')
    print('================================')
    while students:
        if len(students) == 1:
            std = students[0]
            students.remove(std)
            print(f'Poor {std} will have to work alone.')
        else:
            std1 = random.choice(students)
            students.remove(std1)
            print(f'{std1} will pair with', end=' ')
            sys.stdout.flush()
            time.sleep(2)
            std2 = random.choice(students)
            students.remove(std2)
            print(f'{std2}.')
            time.sleep(3)


if __name__ == '__main__':
    if len(sys.argv) != 2 or not os.path.isfile(sys.argv[1]):
        print("Invalid input. Usage: choose_pairs [names_file]")
        exit()
    main(sys.argv[1])
