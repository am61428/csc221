def count_and_say(s):
    """
      >>> count_and_say('1')
      '11'
      >>> count_and_say('11')
      '21'
      >>> count_and_say('21')
      '1211'
    """
    count_vals = []
    val = s[0]
    count = 1
    for char in s[1:]:
        if char == val:
            count += 1
        else:
            count_vals.append((count, val))
            val = char
            count = 1
    s_out = ''
    for count, val in count_vals:
        s_out += str(count) + val
    return s_out


if __name__ == '__main__':
    import doctest
    doctest.testmod()
