def is_neighbor(i, j, row, col, rows, cols):
    """
      >>> is_neighbor(1, 1, 2, 2, 4, 4)
      True
      >>> is_neighbor(0, 0, 0, 0, 4, 4)
      False
      >>> is_neighbor(-1, 0, 0, 0, 4, 4)
      False
      >>> is_neighbor(4, 4, 3, 3, 4, 4)
      False
      >>> is_neighbor(3, 2, 3, 3, 4, 4)
      True
      >>> is_neighbor(3, 1, 3, 3, 4, 4)
      False
    """
    if i == row and j == col: return False
    if i < 0 or j < 0: return False
    if i >= rows or j >= cols: return False
    if abs(row - i) > 1 or abs(col - j) > 1: return False
    return True


rows, cols = [int(val) for val in input().split()]

ingrid = []
for row in range(rows):
    ingrid.append(input().split())

outgrid = []
for row in range(rows):
    outrow = []
    for col in range(cols):
        if ingrid[row][col] == '*':
            outrow.append('*')
        else:
            count = 0
            for i in range(row-1, row+2):
                for j in range(col-1, col+2):
                    if is_neighbor(i, j, row, col, rows, cols):
                       if ingrid[i][j] == '*':
                           count += 1
            outrow.append(str(count))
    outgrid.append(outrow)

for row in outgrid:
    print(' '.join(row))


if __name__ == '__main__':
    import doctest
    doctest.testmod()
