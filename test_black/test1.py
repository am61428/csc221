class Black:
    def __init__(self, name="Black", age=42, goal="to seek the Holy Grail"):
        self.name = name
        self.age = age
        self.goal = goal

    def __str__(self):
        return f"I am {self.name} and my goal is {self.goal}!"
