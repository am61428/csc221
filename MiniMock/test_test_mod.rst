Learning MiniMock
=================

First let's see if we can import test_mod.py and have it call foo and bar.

  >>> import test_mod
  I am foo, and I was sent 42
  name: Arthur
  quest: To seek the holy grail
  <BLANKLINE>

Let's use the functions foo to test mock injection. 

  >>> test_mod.imported_mod.foo("Wow!")
  'I am foo, and I was sent Wow!'
  >>> from minimock import Mock, TraceTracker, Printer
  >>> trace = TraceTracker()
  >>> test_mod.imported_mod = Mock('test_mod.imported_mod', tracker=trace)
  >>> test_mod.imported_mod.foo("Wow!")

Nothing was printed. It worked! Lastly, let's look at the trace.

  >>> trace.dump()
  "Called test_mod.imported_mod.foo('Wow!')\n"
