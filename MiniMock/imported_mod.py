def foo(n):
    return f"I am foo, and I was sent {n}"


def bar(**kwargs):
    s = ""
    for key in kwargs.keys():
        s += f"{key}: {kwargs[key]}\n"
    return s


print(foo(42))
print(bar(name="Arthur", quest="To seek the holy grail"))
