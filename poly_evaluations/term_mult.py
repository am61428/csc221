def scalar_mult(n, p):
    return tuple([n * val for val in p])


def term_mult(term, poly):
    """
      >>> term_mult((2, 3), (4, 0, 0, 2, 0))
      ()
    """
    return scalar_mult(term[0], poly) + (term[1] * (0,))


if __name__ == '__main__':
    import doctest
    doctest.testmod()
