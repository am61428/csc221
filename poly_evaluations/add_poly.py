def add_poly(poly1, poly2):
    """
      >>> add_poly((, , ), (, , , , ))
       
    """
    p1 = list(poly1)
    p2 = list(poly2)
    psum = []
    while p1 and p2:
        psum.insert(0, p1.pop() + p2.pop())
    return tuple(p1 + p2 + psum)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
