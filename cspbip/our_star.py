from turtle import *

SIZE = 200
TURN_ANGLE = 144

space = Screen()
toby = Turtle()
toby.pensize(5)
toby.forward(SIZE)
toby.right(TURN_ANGLE)
toby.forward(SIZE)
toby.right(TURN_ANGLE)
toby.forward(SIZE)
toby.right(TURN_ANGLE)
toby.forward(SIZE)
toby.right(TURN_ANGLE)
toby.forward(SIZE)
toby.right(TURN_ANGLE)
input()
