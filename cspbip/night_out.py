intro = """This program will calculate a restaurant tab for a couple with a
gift certificate, with a restaurant tax of 9%.\n"""

print(intro)

gift_certificate = float(input('Enter the amount of the gift certificate: '))
total_cost = 0

print('\nEnter order costs for the first person.')
total_cost += float(input('Appetizer: '))
total_cost += float(input('Entree: '))
total_cost += float(input('Drinks: '))
total_cost += float(input('Dessert: '))

print('\nEnter order costs for the second person.')
total_cost += float(input('Appetizer: '))
total_cost += float(input('Entree: '))
total_cost += float(input('Drinks: '))
total_cost += float(input('Dessert: '))

print(f"\nOrdered items: ${total_cost:.2f}")
print(f"Restaurant tax: ${0.09 * total_cost:.2f}")
print(f"Tab: ${gift_certificate - 1.09 * total_cost:.2f}")
