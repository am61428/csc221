from PIL import Image


def avgcol(r, g, b):
    return (r + g + b) // 3


img = Image.open('../../data/jelkner.jpg')
width = img.size[0]
height = img.size[1]
img2 = Image.new('RGB', (3 * width, 3 * height))

pixels = img.load()
pixels2 = img2.load()

for col in range(width):
    for row in range(height):
        r, g, b = pixels[col, row]
        pixels2[col, row] = (r, g, b)
        pixels2[col + width, row] = (255 - r, 255 - g, 255 - b)
        gval = avgcol(r, g, b)
        pixels2[col, row + height] = (gval, gval, gval)
        pixels2[col + 2 * width, row] = (gval, 0, 0)
        negpx = 255 - gval
        pixels2[col + width, row + height] = (negpx, negpx, negpx)
        pixels2[col + 2 * width, row + height] = (0, gval, 0)
        pixels2[col, row + 2 * height] = (r, r, r)
        pixels2[col + width, row + 2 * height] = (g, g, g)
        pixels2[col + 2 * width, row + 2 * height] = (b, b, b)

img2.show()
img2.save('too_many_jelkners.jpg')
