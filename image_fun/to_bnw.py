import sys

from PIL import Image


def to_black_and_white(image):
    pixels = image.load()

    for col in range(image.size[0]):
        for row in range(image.size[1]):
            r, g, b = pixels[col, row]
            avg = (r + g + b) // 3
            pixels[col, row] = avg, avg, avg 

    return image


if __name__ == '__main__':
    imgfile = sys.argv[1]
    img = Image.open(imgfile)
    img = to_black_and_white(img)
    img.show()
    input('Press any key to quit...')
