from PIL import Image
from random import choice


img = Image.open('../../data/guido1.jpg')
w, h = img.size[0], img.size[1]
img2 = Image.new('RGB', (w, h))

pixels = img.load()
pixels2 = img2.load()

# Generate a list of tuples of corners of 100 by 64 pixel sections of the image
corners = []
for col_val in range(0, 1000, 100):
    for row_val in range(0, 640, 64):
        corners.append((row_val, col_val))

# Starting with each rectangle corner in the image
for col in range(0, 1000, 100):
    for row in range(0, 640, 64):
        # choose a random corner
        randr, randc = choice(corners)
        # remove it so it doesn't get chosen again
        corners.remove((randr, randc))

        # copy random rect block of orig img to current block in new img
        for co in range(100):
            for ro in range(64):
                pixels2[col + co, row + ro] = pixels[randc + co, randr + ro]


img2.show()
img2.save('scrambled_guido.jpg')
