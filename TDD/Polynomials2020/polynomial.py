import re


def get_terms(polystr):
    """
      >>> get_terms('3x^5-7x^2+11')
      [(3.0, 5), (-7.0, 2), (11.0, 0)]
      >>> get_terms('x^3-x^2+x')
      [(1.0, 3), (-1.0, 2), (1.0, 1)]
      >>> get_terms('5x^4 - 9x^2 + 8x + 40')
      [(5.0, 4), (-9.0, 2), (8.0, 1), (40.0, 0)]
    """
    polystr = polystr.replace(' ', '')
    regex = re.compile(r'([+-]*)((\d+)?(x(\^(\d+))?)|(\d+))')
    raw_terms = regex.findall(polystr)
    terms = []

    # extract terms from capture groups
    for raw_term in raw_terms:
        # handle constant term
        if raw_term[-1]:
            exp = 0
            coeff = float(raw_term[-1])
            if raw_term[0] == '-':
                coeff = -coeff
            terms.append((coeff, exp))
            break
        # handle exponent of linear term
        elif raw_term[3] == 'x':
            exp = 1
        else:
            exp = int(raw_term[5])

        # handle coefficient of 1
        if not raw_term[2]:
            coeff = 1.0
        else:
            coeff = float(raw_term[2])
        # handle negative coefficients
        if raw_term[0] == '-':
            coeff *= -1

        terms.append((coeff, exp))

    return terms


class Polynomial:
    """
      >>> p = Polynomial('3x^2-7x+9')
      >>> print(p)
      3.0x^2 - 7.0x + 9.0
      >>> p2 = Polynomial('5x^4 - 9x^2 + 8x + 40')
      >>> p2
      5.0x^4 - 9.0x^2 + 8.0x + 40.0
    """
    def __init__(self, p):
        if isinstance(p, list):
            self.terms = p
        else:
            self.terms = get_terms(p)

    def __repr__(self):
        poly_str = ''

        for term in self.terms:
            coeff, exp = term
            coeff_str = ''
            # handle negative coefficents
            if coeff < 0:
                coeff_str += ' - '
                coeff *= -1
            else:
                coeff_str += ' + '
            # handle coefficents of 1
            if coeff != 1:
                coeff_str += str(coeff)

            exp_str = ''
            # ignore exp_str for constant term, handle linear term
            if exp > 1:
                exp_str += f'x^{exp}'
            elif exp == 1:
                exp_str += 'x'

            poly_str += coeff_str + exp_str

        return poly_str[3:] if poly_str[1] == '+' else '-' + poly_str[3:]

    def __add__(self, other):
        """
           >>> p1 = Polynomial('2x^2 + 2x + 2')
           >>> p2 = Polynomial('5x^4 - 9x^2 + 8x + 40')
           >>> p3 = p1 + p2
           >>> print(p3)
           5.0x^4 - 7.0x^2 + 10.0x + 42.0
        """
        i1 = 0
        i2 = 0
        sum_poly_terms = []

        while True:
            if self.terms and i1 < len(self.terms):
                term1 = self.terms[i1]
            else:
                term1 = None

            if other.terms and i2 < len(other.terms):
                term2 = other.terms[i2]
            else:
                term2 = None

            if not term1 and not term2:
                return Polynomial(sum_poly_terms)

            if not term1:
                return Polynomial(sum_poly_terms + other.terms[i2:])

            if not term2:
                return Polynomial(sum_poly_terms + self.terms[i1:])

            if term1[1] > term2[1]:
                sum_poly_terms.append(term1)
                i1 += 1
            elif term1[1] < term2[1]:
                sum_poly_terms.append(term2)
                i2 += 1
            else:                            # exponents are equal
                sum_poly_terms.append((term1[0] + term2[0], term1[1]))
                i1 += 1
                i2 += 1


if __name__ == '__main__':
    import doctest
    doctest.testmod()
