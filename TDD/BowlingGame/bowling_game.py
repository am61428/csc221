class BowlingGame:
    """
    Record the rolls in a bowling game and calculate the final score.
    """
    def __init__(self):
        self.frames = []

    def roll(self, pins):
        if not self.frames or len(self.frames[-1]) == 2:
            self.frames.append([pins])
        else:
            self.frames[-1].append(pins)

    def score_frame(self, frame_num):
        total = sum(self.frames[frame_num])
        if total < 10:
            return total
        else:
            return total + self.frames[frame_num+1][0]

    def score(self):
        return sum([self.score_frame(n) for n in range(len(self.frames))])
