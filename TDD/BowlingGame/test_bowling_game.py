from unittest import TestCase

from bowling_game import BowlingGame


class test_bowling_game(TestCase):

    def setUp(self):
        self.game = BowlingGame()

    def roll_many(self, num, pins):
        for roll in range(num):
            self.game.roll(pins)

    def test_gutter_game(self):
        self.roll_many(20, 0)
        self.assertEqual(0, self.game.score())

    def test_all_ones_game(self):
        self.roll_many(20, 1)
        self.assertEqual(20, self.game.score())

    def test_one_spare(self):
        self.game.roll(5)
        self.game.roll(5)  # a spare
        self.game.roll(3)
        self.roll_many(17, 0)
        self.assertEqual(16, self.game.score())

    def test_one_strike(self):
        self.game.roll(10) # strike
        self.game.roll(3)
        self.game.roll(4)
        self.roll_many(16, 0)
        self.assertEqual(24, self.game.score())
