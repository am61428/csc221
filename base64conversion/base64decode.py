#!/usr/bin/env python3
import sys
from my_base64 import bits_to_ints, base64_to_bits, base64_to_binary


if len(sys.argv) != 2:
    print('Invalid input. Please list file name.')
    sys.exit()

infile = sys.argv[1]
base_fname = infile[:-8]
base_extention = infile[-7:-4]
outfile_name = f'{base_fname}.{base_extention}'
outfile = open(outfile_name, 'wb')

with open(infile, 'r') as f:
    base64_to_binary(f, outfile)
