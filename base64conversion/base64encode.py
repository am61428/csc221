#!/usr/bin/env python3
import sys
from my_base64 import bytes_to_bits, bits_to_base64, binary_to_base64


if len(sys.argv) != 2:
    print('Invalid input. Please list file name.')
    sys.exit()

infile = sys.argv[1]
parts = infile.split('.')
outfile_name = f'{parts[0]}_{parts[1]}.txt'
outfile = open(outfile_name, 'w')


with open(infile, 'rb') as f:
    binary_to_base64(f, outfile)
