def convert_to_base(num, base, charset=None):
    """
      >>> convert_to_base(7, 3)
      '21'
      >>> convert_to_base(3, 3)
      '10'
      >>> convert_to_base(12, 4)
      '30'
      >>> convert_to_base(2, 5)
      '2'
      >>> convert_to_base(18, 6)
      '30'
      >>> convert_to_base(12, 16)
      'C'
      >>> convert_to_base(38, 64, 'Base64')
      'm'
    """
    digits = '0123456789ABCDEF'
    b64s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    charsets = {
        'base32hex': '0123456789ABCDEFGHIJKLMNOPQRSTUV',
        'RFC4648': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567',
        'Base64': b64s
        }
    if base > 16:
        if charset:
            digits = charsets[charset]
        elif 16 < base <= 32:
            digits = charsets['base32hex']
        else:
            digits = charsets['Base64']

    bs = ''
    while num != 0:
        bs = digits[num % base] + bs
        num //= base
    if bs == '':
        return '0'
    return bs


if __name__ == '__main__':
    import doctest
    doctest.testmod()
