def bytes_to_bits(bstring):
    """
      >>> bytes_to_bits(b'T')
      '01010100'
      >>> bytes_to_bits(b'Test')
      '01010100011001010111001101110100'
      >>> bytes_to_bits(b'M')
      '01001101'
    """
    bitstr = ''

    for byte in bstring:
        bitstr += format(int(byte), '08b')

    return bitstr


def bits_to_base64(bitstr):
    """
      >>> bits_to_base64('010101000110010101110011')
      'VGVz'
      >>> bits_to_base64('01001101')
      'TQ=='
      >>> bits_to_base64('0100110101100001')
      'TWE='
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    b64str = ''
    ending = ''

    # Check if bitstr length is multiple of 6 and handle cases where it isn't.
    # Since bitstr length will be a multiple of 8 (a byte), there are three
    # cases:
    #   1. len(bitstr) % 6 == 0 when total bytes is a multiple of 3.
    #   2. len(bitstr) % 6 == 2 when total bytes % 3 is 1.
    #   3. len(bitstr) % 6 == 4 when total bytes % 3 is 2.
    # Cases 2 and 3 require special handling.
    if len(bitstr) % 6 == 2:
        ending = '=='          # pad base64 string with ==
        bitstr += '0000'       # make bitstr length a muliple of 6
    elif len(bitstr) % 6 == 4:
        ending = '='           # pad base64 string with =
        bitstr += '00'       # make bitstr length a muliple of 6

    # Convert each 6 bits to a base64 encoded byte
    for i in range(0, len(bitstr), 6):
        b64str += digits[int(bitstr[i:i+6], 2)]

    return b64str + ending


def binary_to_base64(infile, outfile):
    """
      >>> import io
      >>> source = io.BytesIO(b'Tes')
      >>> result = io.StringIO()
      >>> binary_to_base64(source, result)
      >>> result.seek(0)
      0
      >>> result.read()
      'VGVz'
      >>> source = io.BytesIO(b'pleasure.')
      >>> result2 = io.StringIO()
      >>> binary_to_base64(source, result2)
      >>> result2.seek(0)
      0
      >>> result2.read()
      'cGxlYXN1cmUu'
    """
    outfile.write(bits_to_base64(bytes_to_bits(infile.read())))


if __name__ == '__main__':
    import doctest
    doctest.testmod()
