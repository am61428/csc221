from svg_turtle import SvgTurtle 

WIDTH, HEIGHT = 840, 200

t = SvgTurtle(WIDTH, HEIGHT) 
t.width(4)
t.penup()
t.goto(-400, 5)
t.pendown()

for box in range(10):
    for i in range(4):
        t.forward(80)
        t.right(90)
    t.penup()
    t.forward(80)
    t.pendown()

t.color("gray")
t.penup()
t.goto(0, 45)
t.pendown()
t.write("square_numbers", align="center", font=('Courier', 24, 'normal'))

t.color("red")
for index in range(10):
    t.penup()
    t.goto(-360 + 80 * index, 8)
    t.pendown()
    t.write(f"{index}", align="center", font=('Courier', 18, 'bold'))

t.color("green")
for index in range(10):
    t.penup()
    t.goto(-360 + 80 * index, -60)
    t.pendown()
    t.write(f"{index * index}", align="center", font=('Courier', 26, 'bold'))


t.save_as('array1.svg')
