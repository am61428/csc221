from svg_turtle import SvgTurtle 

WIDTH, HEIGHT = 220, 220

t = SvgTurtle(WIDTH, HEIGHT) 

t.penup()
t.backward(100)
t.pendown()

t.color('red', 'yellow')
t.begin_fill()
while True:
    t.forward(200)
    t.left(170)
    if  t.heading() == 0:
        break
t.end_fill()
t.save_as('star.svg')
