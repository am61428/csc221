# SaVaGe Turtle

[SaVaGe Turtle](https://donkirkby.github.io/svg-turtle/) is a Python module
that enables the user to save
[python Turtle graphics](https://docs.python.org/3/library/turtle.html) as
[SVG](https://developer.mozilla.org/en-US/docs/Web/SVG) files.
