# Notes from PCEP Prep Course

* Python permits underscores ``_`` in integer literals, so ``11_111_111`` is a
  legal integer in Python. This feature was introduced in Python 3.6.
