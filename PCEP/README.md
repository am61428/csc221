# Python PCEP - Certified Entry-Level Python Programmer

## Tested Concepts

* Terms and definitions
* Logic and structure
* Syntax and semantics
* Literals and variables
* Numeral systems
* Data types, I/O operations, and control flow
* Data collections
* Exceptions
* Runtime environment

## PCEP 1.1 - Understanding fundamental terms and definitions

* interpreting and the interpreter
* complilation and the compiler
* lexis
* syntax and semantics

## PCEP 1.2 - Understanding Python's logic and structure

* keywords
* instructions
* indenting
* comments

## PCEP 1.3 - Introduce literals and variables into code and use different numeral systems

* Booleans
* integers
* floating-point numbers
* scientific notation
* strings
* binary
* octal
* decimal
* hexadecimal
* variables
* naming conventions
* implementing PEP-8 recommendations

## PCEP 1.4 - Choose operators and data types adequate to the problem

* numeric operators: ** * / % // + -
* string operators: * +
* assignments and shortcut operators
* operators: unary and binary
* priorities and binding
* bitwise operators: ~ & ^ | << >>
* Boolean operators: not and or
* Boolean expresssions
* relational operators: == != > >= < <=
* accuracy of floating-point numbers
* type casting

## PCEP 1.5: Perform Input/Output console operations

* print()
* input()
* functions
* sep= and end= keyword paramenters
* int() and float() functions

## PCEP 2.1: Make decisions and branch the flow with the if instruction

* conditional statements: if, if-else, if-elif, if-elif-else
* multiple conditional statements
* nested conditional statements

## PCEP 2.2: Perform different types of iterations

* the pass instruction
* building loops with while, for, range(), and in
* iterating through sequences
* expanding loops with while-else and for-else
* nesting loops and conditional statements
* controlling loop execution with break and continue

## PCEP 3.1: Collect and process data using lists

* constructing vectors
* indexing and slicing
* the len() function
* basic list methods (append(), insert(), index()) and functions (len(),
  sorted(), etc.)
* the del instruction
* iterating through lists with the for loop
* initializing loops
* in and not in operators
* list comprehensions
* copying and cloning
* lists in lists: matricies and cubes

## PCEP 3.2: Collect and process data using dictionaries

* dictionaries: building, indexing, adding and removing keys
* iterating through dictionaries and their keys and values
* checking the existance of keys
* keys(), items(), and values() methods

## PCEP 3.3: Operate with strings

* constructing strings
* indexing and slicing
* immutability
* escaping using the \ character
* quotes and apostrophes inside strings
* multi-line strings
* basic string functions and methods

## PCEP 4.1: Decompose the code using functions

* defining and invoking user-defined functions and generators
* the return keyword
* returning results
* the None keyword
* recursion

## PCEP 4.2: Organize interaction between the function and its environment

* parameters vs. arguments
* positional, keyword, and mixed argument passing
* default parameter values
* name scopes
* name hiding (shadowing)
* the global keyword

# PCEP 4.3: Python Built-in Exceptions Hierachy

* BaseException, Exception
* SystemExit
* KeyboardInterrupt
* abstractive exceptions
* ArithmeticError
* LookupError along with IndexError and KeyError
* TypeError and ValueError exceptions
* the AssertError exception along with assert keyword

## PCEP 4.4: Basics of Python Exception Handling

* try-except
* try-except Exception
* ordering the except branches
* propagating exceptions through function boundaries
* delegating responsibility for handling exceptions
