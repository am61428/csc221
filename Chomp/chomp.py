from gasp import *


GRID_SIZE = 30
MARGIN = GRID_SIZE

BACKGROUND_COLOR = color.BLACK
WALL_COLOR = "#99E5E5"


class Immovable:
    pass


class Nothing(Immovable):
    pass


class Maze:
    def __init__(self):
        self.have_window = False
        self.game_over = False
        self.get_level()
        self.height = len(self.the_layout)
        self.width = len(self.the_layout[0])
        self.make_window()

    def get_level(self):
        f = open('layout01.dat')
        self.the_layout = [line.rstrip() for line in f.readlines()]

    def make_window(self):
        grid_w = (self.width - 1) * GRID_SIZE
        grid_h = (self.height - 1) * GRID_SIZE
        screen_w = 2 * MARGIN + grid_w
        screen_h = 2 * MARGIN + grid_h
        begin_graphics(screen_w, screen_h, BACKGROUND_COLOR, "Chomp")

    def finished(self):
        return self.game_over

    def play(self):
        answered = input("Are we done yet? ")
        if answered == "y":
            self.game_over = True
        else:
            print("I'm playing")

    def done(self):
        print("I'm done, and here's the_layout:")
        [print(line) for line in self.the_layout]


the_maze = Maze()
while not the_maze.finished():
    the_maze.play()
the_maze.done()
