#!/usr/bin/env python3
import sqlite3

con = sqlite3.connect("empty_tables.db")
cur = con.cursor()

episodes = ['Soup Nazi', 'The Fusilli Jerry']
cur.executemany('insert into episodes (name) values (?)', episodes)
cur.close()
con.commit()
