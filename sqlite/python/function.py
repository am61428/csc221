#!/usr/bin/env python3
import sqlite3

def hello_newman(*args):
    if len(args) > 0:
        names = ', '.join(args)
        return f"Hello {names}"
    return "Hello Jerry"

con = sqlite3.connect(":memory:")
con.create_function("hello_newman", -1, hello_newman)
cur = con.cursor()

cur.execute("select hello_newman()")
print(cur.fetchone()[0])

cur.execute("select hello_newman('Elaine')")
print(cur.fetchone()[0])

cur.execute("select hello_newman('Elaine', 'Jerry')")
print(cur.fetchone()[0])

cur.execute("select hello_newman('Elaine', 'Jerry', 'George')")
print(cur.fetchone()[0])
