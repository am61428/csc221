#!/usr/bin/env python3
import sqlite3


class pysum:
    def __init__(self):
        self.sum = 0

    def step(self, value):
        self.sum += value

    def finalize(self):
        return self.sum


con = sqlite3.connect("seinfeld_foods.db")
con.create_aggregate("pysum", 1, pysum)
cur = con.cursor()
cur.execute("select pysum(id) from foods")
print(cur.fetchone()[0])
