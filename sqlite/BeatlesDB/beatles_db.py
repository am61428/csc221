import json
import sqlite3


def create_db():
    conn = sqlite3.connect('beatles_tracks.db')
    cur = conn.cursor()

    # Make some fresh tables using executescript()
    cur.executescript("""
    DROP TABLE IF EXISTS Songwriter;
    DROP TABLE IF EXISTS Leadsinger;
    DROP TABLE IF EXISTS Album;
    DROP TABLE IF EXISTS Track;
    DROP TABLE IF EXISTS TrackSongwriter;
    DROP TABLE IF EXISTS TrackLeadsinger;

    CREATE TABLE Songwriter (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        name TEXT UNIQUE
    );

    CREATE TABLE Leadsinger (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        name TEXT UNIQUE
    );

    CREATE TABLE Album (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        title TEXT UNIQUE,
        release_date TEXT
    );

    CREATE TABLE Track (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        title TEXT UNIQUE,
        album_id INTEGER,
        play_length INTEGER
    );

    CREATE TABLE TrackSongwriter (
      track_id INTEGER,
      Songwriter_id INTEGER
    );

    CREATE TABLE TrackLeadsinger (
      track_id INTEGER,
      Leadsinger_id INTEGER
    );
    """)

    conn.commit()
    conn.close()


def read_albums_json():
    f = open('albums.json')
    albums = json.load(f)['beatles_albums']
    f.close()
    return albums


if __name__ == "__main__":
    pass 
