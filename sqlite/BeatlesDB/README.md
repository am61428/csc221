# The Beatles Albums Database

This is a little [Python](https://www.python.org/) and
[SQLite](https://www.sqlite.org/index.html) project I'm doing for my Python
programming class.

It will start with a database of tracks from Beatles albums, with information
on the lead singer and song writer for each track.

## Resources

* [Read JSON file using Python](https://www.geeksforgeeks.org/read-json-file-using-python/)
* [SQLite Date & Time](https://www.sqlitetutorial.net/sqlite-date/)
