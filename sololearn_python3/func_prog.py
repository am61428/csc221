def is_prime(n):
    """
      >>> is_prime(2)
      True
      >>> is_prime(12)
      False
      >>> is_prime(13)
      True
      >>> is_prime(1332213)
      False
    """
    if n == 2 or n == 3:
        return True
    if n % 2 == 0 or n < 2:
        return False

    for i in range(3, int(n ** 0.5) + 1, 2):
        if n % i == 0:
            return False

    return True


def get_next_prime_generator():
    last_prime = 2

    while True:
        if is_prime(last_prime):
            yield last_prime

        last_prime += 1


if __name__ == '__main__':
    import doctest
    doctest.testmod()
