try:
    meaning = 42
    result = meaning + 'spam' 
    print('We made it.')
except (ValueError, TypeError):
    print('Ni!')
except ZeroDivisionError:
    print('Swallow?')
except:
    print('Run away!')
