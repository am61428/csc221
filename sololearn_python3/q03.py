try:
    meaning = 42
    result = meaning // 0
    print('We made it.')
except (ValueError, TypeError):
    print('Ni!')
except ZeroDivisionError:
    print('Swallow?')
except:
    print('Run away!')
