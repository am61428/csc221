def only_evens(nums):
    """
      >>> only_evens([3, 8, 5, 4, 12, 7, 2])
      [8, 4, 12, 2]
      >>> my_nums = [4, 7, 19, 22, 42]
      >>> only_evens(my_nums)
      [4, 22, 42]
      >>> my_nums
      [4, 7, 19, 22, 42]
    """
    return [num for num in nums if num % 2 == 0]


def num_even_digits(n):
    """
      >>> num_even_digits(123456)
      3
      >>> num_even_digits(2468)
      4
      >>> num_even_digits(1357)
      0
      >>> num_even_digits(2)
      1
      >>> num_even_digits(20)
      2
    """
    count = 0

    while n:
        d, n = n % 10, n // 10
        if d % 2 == 0:
            count += 1

    return count


def sum_of_squares_of_digits(n):
    """
      >>> sum_of_squares_of_digits(1)
      1
      >>> sum_of_squares_of_digits(9)
      81
      >>> sum_of_squares_of_digits(11)
      2
      >>> sum_of_squares_of_digits(121)
      6
      >>> sum_of_squares_of_digits(987)
      194
    """
    sum_of_digits = 0

    while n:
        d, n = n % 10, n // 10
        sum_of_digits += d ** 2

    return sum_of_digits


def lots_of_letters(word):
    """
      >>> lots_of_letters('Lidia')
      'Liidddiiiiaaaaa'
      >>> lots_of_letters('Python')
      'Pyyttthhhhooooonnnnnn'
      >>> lots_of_letters('')
      ''
      >>> lots_of_letters('1')
      '1'
    """
    s = ''

    for index, letter in enumerate(word):
        s += (index + 1) * letter

    return s


def gcf(m, n):
    """
    Use the Euclidean algorithm as described here in
    https://en.wikipedia.org/wiki/Euclidean_algorithm to compute the greatest
    common factor (gcf) of two numbers.

      >>> gcf(10, 25)
      5
      >>> gcf(8, 12)
      4
      >>> gcf(5, 12)
      1
      >>> gcf(24, 12)
      12
    """
    # make sure m is larger than n
    if n > m:
        m, n = n, m

    while n:
        m, n = n, m % n

    return m


def is_prime(n):
    """
    Return True if n is a prime number or False if it is not.

      >>> is_prime(2)
      True
      >>> is_prime(5)
      True
      >>> is_prime(15)
      False
      >>> is_prime(55)
      False
      >>> is_prime(53)
      True
    """
    # Make sure n is greater than 1
    assert(n > 1), "n must be greater than 1"

    # If n is 2 or 3, return True, if it is a multiple of 2, return False
    if n == 2 or n == 3:
        return True
    if n % 2 == 0:
        return False

    # Otherwise, return True if there are no odd numbers between
    # 3 and the next integer from the square root of n that divide n evenly,
    # and False otherwise
    for x in range(3, int(n**0.5)+1, 2):
        if n % x == 0:
            return False
    return True


if __name__ == '__main__':
    import doctest
    doctest.testmod()
