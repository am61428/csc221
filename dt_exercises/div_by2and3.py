#
# Write a function named by divisible_by_2and3(start=2, stop=20) that prints
# a table of each value between start and stop and whether it is divisible
# by 2, 3, both 2 and 3, or neither 2 or 3. The function should only print
# the table heading if called with a single argument of None.
#


def divisible_by_2and3(start=2, stop=20):
    """
      >>> divisible_by_2and3(None)
      Num   Div by 2 and/or 3?
      ---   ------------------
      >>> divisible_by_2and3(2, 2)
      Num   Div by 2 and/or 3?
      ---   ------------------
      2            by 2
      >>> divisible_by_2and3(2, 3)
      Num   Div by 2 and/or 3?
      ---   ------------------
      2            by 2
      3            by 3
      >>> divisible_by_2and3(2, 4)
      Num   Div by 2 and/or 3?
      ---   ------------------
      2            by 2
      3            by 3
      4            by 2
      >>> divisible_by_2and3(2, 6)
      Num   Div by 2 and/or 3?
      ---   ------------------
      2            by 2
      3            by 3
      4            by 2
      5            neither
      6            both
      >>> divisible_by_2and3()
      Num   Div by 2 and/or 3?
      ---   ------------------
      2            by 2
      3            by 3
      4            by 2
      5            neither
      6            both
      7            neither
      8            by 2
      9            by 3
      10           by 2
      11           neither
      12           both
      13           neither
      14           by 2
      15           by 3
      16           by 2
      17           neither
      18           both
      19           neither
      20           by 2
    """
    print('Num   Div by 2 and/or 3?')
    print('---   ------------------')
    if start:
        for num in range(start, stop+1):
            if num % 6 == 0:
                message = 'both'
            elif num % 3 == 0:
                message = 'by 3'
            elif num % 2 == 0:
                message = 'by 2'
            else:
                message = 'neither'
            print(f'{num:<2}           {message}')


if __name__ == '__main__':
    import doctest
    doctest.testmod()
