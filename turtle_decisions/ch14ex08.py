from turtle import *

space = Screen()

height = space.window_height()
width = space.window_width()
max_y = height // 2
max_x = width // 2

sue = Turtle()
sue.pensize(10)

for line_num in range(4):
    sue.penup()
    if line_num % 2 == 0:
        sue.color('yellow')
        sue.goto(-1 * max_x, line_num * 10)
        sue.pendown()
        sue.forward(width)
        sue.left(90)
    else:
        sue.color('black')
        sue.goto(line_num * 10, -1 * max_y)
        sue.pendown()
        sue.forward(height)
        sue.right(90)

input()
