class Fraction:
    """
      >>> f = Fraction(3, 4)
      >>> print(f)
      3/4
      >>> f2 = Fraction('7/8')
      >>> f2.n
      7
      >>> f2.d
      8
      >>> print(f * f2)
      21/32
    """
    def __init__(self, n=1, d=1):
        if type(n) == type('') and '/' in n:
            parts = n.split('/')
            self.n, self.d= int(parts[0]), int(parts[1])
        else:
            self.n= n
            self.d= d

    def __str__(self):
        return f'{self.n}/{self.d}'

    def __mul__(self, other):
        return Fraction(self.n * other.n, self.d * other.d)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
