def add_poly(p1, p2):
    """
      >>> add_poly([3, 5, 6], [4, 0, -3])
      [7, 5, 3]
      >>> add_poly([5, 6], [4, -4])
      [9, 2]
      >>> add_poly([6, 2, 5, 6], [-5, 4, 0, -3])
      [1, 6, 5, 3]
      >>> add_poly([4, 2, 8, 6], [-5, 0, -3])
      [4, -3, 8, 3]
      >>> add_poly([5, 9, 3, 2, 7], [5, -3])
      [5, 9, 3, 7, 4]
    """
    poly_sum = []

    if len(p1) < len(p2):
        p1, p2 = p2, p1

    p2 = (len(p1) - len(p2)) * [0] + p2

    for i in range(len(p1)):
        poly_sum.append(p1[i] + p2[i])

    return poly_sum


if __name__ == '__main__':
    import doctest
    doctest.testmod()
