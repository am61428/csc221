def write_poly(poly):
    """
      >>> write_poly([5])
      '5'
      >>> write_poly([2, 3, 4, 5])
      '2x^3 + 3x^2 + 4x + 5'
      >>> write_poly([3, 0])
      '3x'
      >>> write_poly([-2, 0, 4, -5])
      '-2x^3 + 4x + -5'
      >>> write_poly([1, 0, 1, 1])
      'x^3 + x + 1'
    """
    poly_str = ''

    # Handle the 1st and 0th degree terms specially
    # First check for constant
    if len(poly) == 1:
        return str(poly[0])
    # create poly string with linear term
    poly_str = f'{poly[-2]}x' if poly[-2] != 1 else 'x'
    # if constant term is not zero, append it
    if poly[-1]:
        poly_str += f' + {poly[-1]}'

    # prepend remaining terms from lowest to highest degree
    # since we are moving from end of list toward front, index of degree
    # term is -(degree+1)
    for degree in range(2, len(poly)):
        # the coefficient is zero, skip it
        if poly[-(degree+1)] == 0:
            continue
        # otherwise, prepend next term, moving from end of list toward
        # read the coefficient
        coeff = poly[-(degree+1)]
        # check if the coefficient is 1
        if coeff == 1:
            coeff = ''
        poly_str = f'{coeff}x^{degree} + ' + poly_str

    return poly_str


def read_term(term_str):
    """
      >>> read_term('4x^5')
      (4, 5)
      >>> read_term('7x')
      (7, 1)
      >>> read_term('8.5')
      (8.5, 0)
    """
    # handle most common case - terms of degree 2 or higher
    if 'x^' in term_str:
        coeff_str = term_str[:term_str.find('x')]
        coeff = float(coeff_str) if '.' in coeff_str else int(coeff_str)
        degree = int(term_str[term_str.find('^')+1:])
        return (coeff, degree)
    # handle linear case
    if 'x' in term_str:
        coeff = float(term_str[:-1]) if '.' in term_str else int(term_str[:-1])
        return (coeff, 1)
    # handle the constant case
    return (float(term_str) if '.' in term_str else int(term_str), 0)


def read_poly(poly_str):
    """
      >>> read_poly('-2x^3 + 4x + -5')
      [-2, 0, 4, -5]
      >>> read_poly('2x^3 + 3x^2 + 4x + 5')
      [2, 3, 4, 5]
    """
    # split the polynomial string into a list of term strings
    terms = poly_str.split(' + ')

    # find the degree of the polynomial
    # for polynomials of degree 2 or higher, search for the '^' and slice off
    # the exponent to the right of it
    if '^' in terms[0]:
        degree = int(terms[0][terms[0].find('^')+1:])
    elif 'x' in terms[0]:
        degree = 1                 # it's a linear polynomial
    else:
        degree = 0                 # it's a constant polynomial

    # create a list with places for degree + 1 coefficients
    poly = (degree + 1) * [0]

    # iterate over the terms, setting each coefficient in the poly list
    for term_str in terms:
        coeff, exp = read_term(term_str)
        poly[degree-exp] = coeff

    return poly


def add_poly(p1, p2):
    """
      >>> add_poly([3, 5, 6], [4, 0, -3])
      [7, 5, 3]
      >>> add_poly([5, 6], [4, -4])
      [9, 2]
      >>> add_poly([6, 2, 5, 6], [-5, 4, 0, -3])
      [1, 6, 5, 3]
      >>> add_poly([4, 2, 8, 6], [-5, 0, -3])
      [4, -3, 8, 3]
      >>> p1 = '5x^4 + 9x^3 + 3x^2 + 2x + 7'
      >>> p2 = '5x + -3'
      >>> write_poly(add_poly(read_poly(p1), read_poly(p2)))
      '5x^4 + 9x^3 + 3x^2 + 7x + 4'
    """
    poly_sum = []

    if len(p1) < len(p2):
        p1, p2 = p2, p1

    p2 = (len(p1) - len(p2)) * [0] + p2

    for i in range(len(p1)):
        poly_sum.append(p1[i] + p2[i])

    return poly_sum


if __name__ == '__main__':
    import doctest
    doctest.testmod()
