def write_poly(poly):
    """
      >>> write_poly((5,))
      '5'
      >>> write_poly((2, 3, 4, 5))
      '2x^3 + 3x^2 + 4x + 5'
      >>> write_poly((3, 0))
      '3x'
      >>> write_poly((-2, 0, 4, -5))
      '-2x^3 + 4x + -5'
      >>> write_poly((1, 1, 1, 1))
      'x^3 + x^2 + x + 1'
    """
    poly_str = ''

    # Handle the 1st and 0th degree terms specially
    # First check for constant
    if len(poly) == 1:
        return str(poly[0])
    # create poly string with linear term
    # if coefficient is 1, don't print it
    coeff = poly[-2] if poly[-2] != 1 else ''
    poly_str = f'{coeff}x'
    # if constant term is not zero, append it
    if poly[-1]:
        poly_str += f' + {poly[-1]}'

    # prepend remaining terms from lowest to highest degree
    # since we are moving from end of list toward front, index of degree
    # term is -(degree+1)
    for degree in range(2, len(poly)):
        # the coefficient is zero, skip it
        if poly[-(degree+1)] == 0:
            continue
        # if the coefficient is 1, don't write it
        coeff = poly[-(degree+1)] if poly[-(degree+1)] != 1 else ''
        # otherwise, prepend next term, moving from end of list toward
        poly_str = f'{coeff}x^{degree} + ' + poly_str

    return poly_str


def read_term(term_str):
    """
      >>> read_term('4x^5')
      (4, 5)
      >>> read_term('7x')
      (7, 1)
      >>> read_term('8.5')
      (8.5, 0)
    """
    # handle most common case - terms of degree 2 or higher
    if 'x^' in term_str:
        coeff_str = term_str[:term_str.find('x')]
        coeff = float(coeff_str) if '.' in coeff_str else int(coeff_str)
        degree = int(term_str[term_str.find('^')+1:])
        return (coeff, degree)
    # handle linear case
    if 'x' in term_str:
        coeff = float(term_str[:-1]) if '.' in term_str else int(term_str[:-1])
        return (coeff, 1)
    # handle the constant case
    return (float(term_str) if '.' in term_str else int(term_str), 0)


def read_poly(poly_str):
    """
      >>> read_poly('-2x^3 + 4x + -5')
      (-2, 0, 4, -5)
      >>> read_poly('2x^3 + 3x^2 + 4x + 5')
      (2, 3, 4, 5)
      >>> read_poly('42')
      (42,)
      >>> read_poly('4x^3 + 2')
      (4, 0, 0, 2)
      >>> read_poly('2.2x')
      (2.2, 0)
    """
    # split the polynomial string into a list of term strings
    terms = poly_str.split(' + ')

    # find the degree of the polynomial
    # for polynomials of degree 2 or higher, search for the '^' and slice off
    # the exponent to the right of it
    if '^' in terms[0]:
        degree = int(terms[0][terms[0].find('^')+1:])
    elif 'x' in terms[0]:
        degree = 1                 # it's a linear polynomial
    else:
        degree = 0                 # it's a constant polynomial

    # create a list with places for degree + 1 coefficients
    poly = (degree + 1) * [0]

    # iterate over the terms, setting each coefficient in the poly list
    for term_str in terms:
        coeff, exp = read_term(term_str)
        poly[degree-exp] = coeff

    return tuple(poly)


def add_poly(poly1, poly2):
    """
      >>> add_poly((4, 0, -5, 11), (5, 2, -7))
      (4, 5, -3, 4)
      >>> add_poly((3, 4), (4, 5))
      (7, 9)
      >>> add_poly((2, 4, 5, 6, 7), (7, 4, 2))
      (2, 4, 12, 10, 9)
      >>> add_poly((2,), (4, 7, 3))
      (4, 7, 5)
      >>> add_poly((6, 0), (1, 2))
      (7, 2)
      >>> add_poly((0.5, 4), (4, -0.5))
      (4.5, 3.5)
    """
    p1 = list(poly1)
    p2 = list(poly2)
    psum = []

    # keep adding corresponding coefficients until one runs out
    while p1 and p2:
        psum.insert(0, p1.pop() + p2.pop())

    # prepend the remaining coefficients of non-empty poly (if there is one)
    # together with the empty one (or both) to the sum poly and return tuple
    return tuple(p1 + p2 + psum)


def scalar_mult(n, p):
    """
      >>> scalar_mult(5, (4,))
      (20,)
      >>> scalar_mult(3, (2, -5, 1, 0))
      (6, -15, 3, 0)
    """
    return tuple([n * val for val in p])


def term_mult(term, poly):
    """
      >>> term_mult((3, 4), (1, 2, 3, 4))
      (3, 6, 9, 12, 0, 0, 0, 0)
      >>> term_mult((4, 1), (2, 2, 7))
      (8, 8, 28, 0)
    """
    return scalar_mult(term[0], poly) + (term[1] * (0,))


def mult_poly(p1, p2):
    """
      >>> mult_poly((1, 3), (1, 3))
      (1, 6, 9)
      >>> mult_poly((2, 3, 2), (4, 1))
      (8, 14, 11, 2)
      >>> mult_poly((1, 0, 0), (3, 2, 1))
      (3, 2, 1, 0, 0)
      >>> mult_poly((2, 4), (3, 0, 0, 6, -7, 8))
      (6, 12, 0, 12, 10, -12, 32)
    """
    # create an empty tuple for the product
    prod = ()

    # set p1 to the shorter polynomial
    if len(p1) > len(p2):
        p1, p2 = p2, p1

    degree = len(p1) - 1

    for i, coeff in enumerate(p1):
        term = (coeff, degree - i)
        prod = add_poly(term_mult(term, p2), prod)

    return prod


if __name__ == "__main__":
    import doctest
    doctest.testmod()
