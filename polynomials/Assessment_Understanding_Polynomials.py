QRS = """
Question number: {0}
==================
{1}

{2}

Result
======
"""


def parse_question(raw_question):
    """
      >>> q = '5\\nWhat\\'s up?\\n-----\\nprint(\\"Hello World!\\")'
      >>> parse_question(q)
      (5, "What's up?", 'print("Hello World!")')
    """
    parts = raw_question.split('\n')
    result = (int(parts[0].strip()),)
    qtext = ''
    for i, s in enumerate(parts[1:]):
        if s == '-----':
            break
        qtext += s
    result += (qtext,) + ('\n'.join(parts[i+2:]),)
    return result


def print_quiz_results(questions_file):
    qf = open(questions_file, 'r')
    raw_questions = qf.read().split('+++++')
    for question in raw_questions:
        qnum, qtext, qcode = parse_question(question.strip())
        print(QRS.format(qnum, qtext, qcode), end='')
        exec(qcode.strip(), {})


print_quiz_results('Assessment_Understanding_Polynomials_Questions.txt')


if __name__ == '__main__':
    import doctest
    doctest.testmod()
