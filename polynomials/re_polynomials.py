import re


def print_term(coeff, exp):
    """
      >>> print_term(4, 5)
      '4x^5'
      >>> print_term(1, 3)
      'x^3'
      >>> print_term(2, 1)
      '2x'
      >>> print_term(3, 0)
      '3'
      >>> print_term(-3, 4)
      '3x^4'
      >>> print_term(1, 1)
      'x'
      >>> print_term(0, 7)
      ''
    """
    if coeff == 0:
        return ''

    part1 = str(abs(coeff)) if abs(coeff) != 1 else ''

    if exp == 0:
        part2 = ''
    elif exp == 1:
        part2 = 'x'
    else:
        part2 = 'x^' + str(exp)

    return '{}{}'.format(part1, part2)


def print_poly(p):
    """
      >>> print_poly([4, 3, 2])
      '4x^2 + 3x + 2'
      >>> print_poly([6, 0, 5])
      '6x^2 + 5'
      >>> print_poly([7, 0, -3, 5])
      '7x^3 - 3x + 5'
      >>> print_poly([1, -1, 0, 0, -3, 2])
      'x^5 - x^4 - 3x + 2'
      >>> print_poly([7, 0, -3, 5, 0])
      '7x^4 - 3x^2 + 5x'
    """
    exp = len(p) - 1
    poly_str = print_term(p[0], exp)

    for coeff in p[1:]:
        exp -= 1
        if coeff > 0:
            opstr = ' + '
        elif coeff < 0:
            opstr = ' - '
        else:
            opstr = ''
        poly_str += opstr + print_term(coeff, exp)

    return poly_str


def read_terms(polystr):
    """
      >>> read_terms('4x^2 + 3x + 2')
      [(4, 2), (3, 1), (2, 0)]
      >>> read_terms('-3x^3 - 2x^2 + 5x - 6')
      [(-3, 3), (-2, 2), (5, 1), (-6, 0)]
      >>> read_terms('x^3 - x^2 + x - 1')
      [(1, 3), (-1, 2), (1, 1), (-1, 0)]
    """
    term_matcher = re.compile("([-+]?) *(?:(\d)*(x)(?:\^(\d+))?|(\d+))")
    return [(int(match[1] or match[4] or 1) * (-1 if match[0] == '-' else 1),
             int(match[3] or 1 if match[2] else match[1] or 0)
             )
            for match in term_matcher.findall(polystr)]


def read_poly(polystr):
    """
      >>> read_poly('4x^2 + 3x + 2')
      [4, 3, 2]
      >>> read_poly('-6x^5 + 3x^2 - 2x + 8')
      [-6, 0, 0, 3, -2, 8]
    """
    terms = read_terms(polystr)
    poly = [terms[0][0]]
    exp = terms[0][1]

    for term in terms[1:]:
        while exp > term[1] + 1:
            poly.append(0)
            exp -= 1
        poly.append(term[0])
        exp = term[1]

    return poly


def read_poly2(polystr):
    """
      >>> read_poly2('4x^2 + 3x + 2')
      [4, 3, 2]
      >>> read_poly2('-6x^5 + 3x^2 - 2x + 8')
      [-6, 0, 0, 3, -2, 8]
    """
    terms = read_terms(polystr)
    degree = terms[0][1]
    poly = [0] * (degree + 1)

    for term in terms:
        pos = degree - term[1]
        poly[pos] = term[0]

    return poly


def add_poly(p1, p2):
    """
      >>> add_poly([3, 2, 1], [1, 2, 0])
      [4, 4, 1]
      >>> add_poly([1, 0, 2, 3, 1], [1, 2, 0, 0])
      [1, 1, 4, 3, 1]
    """
    if len(p1) < len(p2):        # set p1 to the longest
        p1, p2 = p2, p1
    extra = len(p1) - len(p2)    # find index of exta part of p1
    sum_poly = p1[:extra]        # slice off the extra from p1
    p1_rest = p1[extra:]         # get the rest of p1
    # sum the common parts of p1 and p2 and join to extra part of p1
    sum_poly += [p1_rest[i] + p2[i] for i in range(len(p2))]
    return sum_poly


def scalar_mult(n, p):
    """
      >>> scalar_mult(3, [2, 1, -4])
      [6, 3, -12]
    """
    return [n * e for e in p]


def term_mult(coeff, exp, p):
    """
      >>> term_mult(3, 4, [2, 1, -4])
      [6, 3, -12, 0, 0, 0, 0]
    """
    return [coeff * elem for elem in p] + exp * [0]


def mult_poly(p1, p2):
    """
      >>> mult_poly([3, 1], [2, 5])
      [6, 17, 5]
      >>> mult_poly([3, 1, 2], [2, 0, 5])
      [6, 2, 19, 5, 10]
    """
    prod = []
    e = len(p1) - 1

    for c in p1:
        prod = add_poly(term_mult(c, e, p2), prod)
        e -= 1

    return prod


if __name__ == '__main__':
    import doctest
    doctest.testmod()
