def term_mult(t, p):
    """
       >>> term_mult((4, 2), (3, 0, 6, 0))
       (12, 0, 24, 0, 0, 0)
       >>> term_mult((5, 6), (9, 6, 2, 1, 0, 5))
       (45, 30, 10, 5, 0, 25, 0, 0, 0, 0, 0, 0)
       >>> term_mult((2, 3), (8, 4, 2, 11))
       (16, 8, 4, 22, 0, 0, 0)
    """
    return tuple([t[0] * coeff for coeff in p] + t[1] * [0])


if __name__=='__main__':
    import doctest
    doctest.testmod()
