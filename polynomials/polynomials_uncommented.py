def write_poly(poly):
    poly_str = ''
    if len(poly) == 1:
        return str(poly[0])
    coeff = poly[-2] if poly[-2] != 1 else ''
    poly_str = f'{coeff}x'
    if poly[-1]:
        poly_str += f' + {poly[-1]}'
    for degree in range(2, len(poly)):
        if poly[-(degree+1)] == 0:
            continue
        coeff = poly[-(degree+1)] if poly[-(degree+1)] != 1 else ''
        poly_str = f'{coeff}x^{degree} + ' + poly_str
    return poly_str


def read_term(term_str):
    if 'x^' in term_str:
        coeff_str = term_str[:term_str.find('x')]
        coeff = float(coeff_str) if '.' in coeff_str else int(coeff_str)
        degree = int(term_str[term_str.find('^')+1:])
        return (coeff, degree)
    if 'x' in term_str:
        coeff = float(term_str[:-1]) if '.' in term_str else int(term_str[:-1])
        return (coeff, 1)
    return (float(term_str) if '.' in term_str else int(term_str), 0)


def read_poly(poly_str):
    terms = poly_str.split(' + ')
    if '^' in terms[0]:
        degree = int(terms[0][terms[0].find('^')+1:])
    elif 'x' in terms[0]:
        degree = 1
    else:
        degree = 0
    poly = (degree + 1) * [0]
    for term_str in terms:
        coeff, exp = read_term(term_str)
        poly[degree-exp] = coeff
    return tuple(poly)


def add_poly(poly1, poly2):
    p1 = list(poly1)
    p2 = list(poly2)
    psum = []
    while p1 and p2:
        psum.insert(0, p1.pop() + p2.pop())
    return tuple(p1 + p2 + psum)


def scalar_mult(n, p):
    return tuple([n * val for val in p])


def term_mult(term, poly):
    return scalar_mult(term[0], poly) + (term[1] * (0,))


def mult_poly(p1, p2):
    prod = ()
    if len(p1) > len(p2):
        p1, p2 = p2, p1
    degree = len(p1) - 1
    for i, coeff in enumerate(p1):
        term = (coeff, degree - i)
        prod = add_poly(term_mult(term, p2), prod)
    return prod
