def add_poly(poly1, poly2):
    """
      >>> add_poly((4, 0, -5, 11), (5, 2, -7))
      (4, 5, -3, 4)
      >>> add_poly((3, 4), (4, 5))
      (7, 9)
      >>> add_poly((2, 4, 5, 6, 7), (7, 4, 2))
      (2, 4, 12, 10, 9)
      >>> add_poly((2,), (4, 7, 3))
      (4, 7, 5)
      >>> add_poly((6, 0), (1, 2))
      (7, 2)
      >>> add_poly((0.5, 4), (4, -0.5))
      (4.5, 3.5)
    """
    p1 = list(poly1)
    p2 = list(poly2)
    psum = []

    while p1 and p2:
        psum.insert(0, p1.pop() + p2.pop())

    # Prepend the remaining coefficients of longer list and empty one
    return tuple(p1 + p2 + psum)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
