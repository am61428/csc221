def term_mult(term, poly):
    """
      >>> term_mult((3, 1), (1, 4, 2))
      (3, 12, 6, 0)
      >>> term_mult((2, 3), (1, 4, 2))
      (2, 8, 4, 0, 0, 0)
      >>> term_mult((6, 5), (5, 6, 3, 4))
      (30, 36, 18, 24, 0, 0, 0, 0, 0)
    """
    degree = term[1]
    for i in range(term[1]):
        poly = poly + (0,)
    return tuple([term[0] * val for val in poly])


if __name__ == '__main__':
    import doctest
    doctest.testmod()
