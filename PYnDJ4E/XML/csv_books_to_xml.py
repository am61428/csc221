import csv

books = list(csv.reader(open('books.csv'), delimiter=';'))
# print(books)

book_template = """<book>
    <title>{title}</title>
    <class>{cls}</class>
    <subclass>{subcls}</subclass>
    <cutter>{cut}</cutter>
    <supple>{sppl}</supple>
    <isbn>{isbn}</isbn>
    <status>{stat}</status>
</book>"""

xml_books_file = open('books.xml', 'w')
xml_books_file.write('<?xml version="1.0" encoding="UTF-8"?>')
xml_books_file.write('\n<home_library>')

for book in books[1:]:
    parts = {}
    parts['title'] = book[0]
    parts['cls'] = book[1]
    parts['subcls'] = book[2]
    parts['cut'] = book[3]
    parts['sppl'] = book[4]
    parts['isbn'] = book[5]
    parts['stat'] = book[6]
    xml_books_file.write('\n' + book_template.format(**parts))

xml_books_file.write('\n</home_library>')
xml_books_file.close()
