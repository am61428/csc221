# Program to load xml books file and explore Python's xml library
# Resource: https://www.datacamp.com/community/tutorials/python-xml-elementtree
import xml.etree.ElementTree as ET

books_tree = ET.parse('books.xml')

library = books_tree.getroot()

for book in library:
    # print(book.getchildren())
    for child in book:
        print(f'{child.tag}: {child.text}')
