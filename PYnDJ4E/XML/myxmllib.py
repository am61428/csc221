import xml.etree.ElementTree as ET

def getdata(fname):
    try:
        f = open(fname, 'r')
        etree = ET.fromstring(f.read())
        f.close()
    except:
        print('Failed to open file and parse xml data.')
        return

    return etree
