from socket import *

def create_server():
    serversock = socket(AF_INET, SOCK_STREAM)
    try:
        serversock.bind(('localhost', 9000))
        serversock.listen(5)

        while True:
            (clientsock, address) = serversock.accept()

            recved = clientsock.recv(5000).decode()
            pieces = recved.split('\n')
            if (len(pieces) > 0):
                print(pieces[0])

            data = 'HTTP/1.1 200 OK\r\n'
            data += 'Context-Type: text/html; charset=utf-8\r\n\r\n'
            data += '<!DOCTYPE html>\n<html lang="en">\n<head>'
            data += '<meta charset="utf-8">\n<title>Simplest Web Server'
            data += '</title>\n</head>\n<body>\n<h1>Hello, from the Simplest'
            data += ' Web Server!</h1>\n</body></html>\r\n\r\n'

            clientsock.sendall(data.encode())
            clientsock.shutdown(SHUT_WR)

    except KeyboardInterrupt:
        print('\nShutting down...\n');
    except Exception as exc:
        print('Error:\n', exc)

    serversock.close()

print('Access http://localhost:9000')
create_server()
