class PartyAnimal:
    x = 0
    name = "Party Animal!"

    def __init__(self, nam):
        self.name = nam
        print(self.name, "initiated")

    def party(self):
        self.x += 1
        print(self.name, "party count", self.x)


class FootballFan(PartyAnimal):
    points = 0

    def touchdown(self):
        self.points += 7
        self.party()
        print(self.name, "points", self.points)
