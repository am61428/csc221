from partyanimals import PartyAnimal, FootballFan


sue = PartyAnimal('Sue')
bob = PartyAnimal('Bob')
becky = FootballFan('Becky')

sue.party()
bob.party()
becky.touchdown()
becky.party()

print(FootballFan.points)
print(FootballFan.x)
print(FootballFan.name)
