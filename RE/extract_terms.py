# Resources:
# https://stackoverflow.com/questions/28859919/java-regex-separate-degree-coeff-of-polynomial
# https://topic.alibabacloud.com/a/use-a-regular-expression-font-colorredregexfont-to-match-polynomials-polynomial-regexpolynomial_1_31_32668196.html
import re


def get_terms(polynomial):
    regex = re.compile(r"([+-]*)(\d+)?(x(\^(\d+))?)?")
    
    return regex.findall(polynomial) 


print(get_terms('5x^9-4x^7+8x^4-16x+42'))
