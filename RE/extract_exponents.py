# From https://stackoverflow.com +
# /questions/50193186/using-re-finditer-to-extract-numbers-from-a-polynomial
import re


def get_exponents(polynomial):
    regex = re.compile(r"x\^[1-9]*")
    return regex.findall(polynomial)


print(get_exponents('2x^3+4x^2+8x-16'))
