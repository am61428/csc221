# From https://stackoverflow.com +
# /questions/50193186/using-re-finditer-to-extract-numbers-from-a-polynomial
import re


def get_coeffs(polynomial):
    regex = re.compile(r"(?:[+-]?\d*\.\d*|\+?(-?\d+))")
    return regex.findall(polynomial)


print(get_coeffs('2x^3+4x^2+8x-16'))
