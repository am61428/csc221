#
# How many empty lines will the following snippet send to the console?
#
l = [[c for c in range(r)] for r in range(3)]
# print(l)
for x in l:
    if len(x) < 2:
        print()
