def I2(n):
    s = '+'
    for i in range(n):
        s += s
        yield s


for x in I2(2):
    print(x, end='')
