import re


def read_poly(poly_str):
    """
      >>> read_poly('-2x^3 + 4x + -5')
      {3: -2, 1: 4, 0: -5}
      >>> read_poly('2x^3 + 3x^2 + 4x + 5')
      {3: 2, 2: 3, 1: 4, 0: 5}
      >>> read_poly('42')
      {0: 42}
      >>> read_poly('4x^3 + 2')
      {3: 4, 0: 2}
      >>> read_poly('2.2x')
      {1: 2.2}
    """
    # Use the sub method from re module to remove representation exceptions
    # Add x^0 to constant terms
    if poly_str[-1].isdigit():
        poly_str += 'x^0'
    # Handle missing 1 coefficents
    poly_str = re.sub('(^|\s)x', '1x', poly_str)
    # Add ^1 to linear terms
    poly_str = re.sub('x($|\s)', 'x^1 ', poly_str)
    poly_str = poly_str.strip()
    # Now seperate the terms
    terms = poly_str.split(' + ')

    # Add the terms to the polynomial dictionary
    poly = {}

    for term in terms:
        exp = int(term[term.find('^')+1:])
        coeff_str = term[:term.find('x')]
        coeff = float(coeff_str) if '.' in coeff_str else int(coeff_str)
        poly[exp] = coeff

    return poly


def add_poly(p1, p2):
    """
      >>> add_poly({2: 1, 1: 3, 0: 5}, {2: 2, 1: 2, 0: 2})
      {2: 3, 1: 5, 0: 7}
      >>> psum = add_poly({3: 2, 2: 3, 0: 5}, {2: 2, 1: 4})
      >>> psum[3] == 2 and psum[2] == 5 and psum[1] == 4 and psum[0] == 5
      True
    """
    sum_poly = p1.copy()
    
    for key in p2.keys():
        if key in sum_poly:
            sum_poly[key] += p2[key]
        else:
            sum_poly[key] = p2[key]

    return sum_poly


def scalar_mult(n, p):
    pass


def term_mult(term, poly):
    pass


def mult_poly(p1, p2):
    pass


if __name__ == '__main__':
    import doctest
    doctest.testmod()
