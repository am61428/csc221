import string

def cost_to_swap(word1, word2):
    """
      >>> cost_to_swap('agnes', 'heard')
      10
      >>> cost_to_swap('minions', 'unicorn')
      -1
      >>> cost_to_swap('victor', 'vector')
      4
      >>> cost_to_swap('sweat', 'waste')
      0
    """
    assert all([ch in string.ascii_lowercase for ch in word1 + word2])
    cost = 0

    for pos in range(len(word1)):
        cost += (ord(word1[pos]) - ord(word2[pos]))

    return cost


if __name__ == '__main__':
    import doctest
    doctest.testmod()
