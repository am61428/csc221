from carnival_game import cost_to_swap


f = open('input.txt', 'r')
pairs = f.readlines()

for pair in pairs:
    word1, word2 = pair[:-1].split()
    result = cost_to_swap(word1, word2)

    if result > 0:
        ending = f'earned {result} coins.'
    elif result < 0:
        ending = f'cost {-result} coins.'
    else:
        ending = 'was FREE.'

    if abs(result) == 1:
        ending = ending[:-2] + '.'

    print(f'Swapping letters to make {word1} look like {word2} {ending}')
