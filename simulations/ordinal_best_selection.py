import random


def look(a_list, percent):
    # compute largest look index
    last_look = int(percent * len(a_list))
    # set best seen so far to first one
    best = a_list[0]
    index = 0

    # find the best in the look phase
    while index <= last_look:
        if a_list[index] > best:
            best = a_list[index]
        index += 1

    return best


def leap(a_list, percent, best_look):
    # set index to first leap index
    index = int(percent * len(a_list)) + 1
    # look for better than best so far, then leap when found
    while index < len(a_list):
        if a_list[index] > best_look:
            return a_list[index]
        index += 1

    return a_list[-1]


bottom = random.randrange(980)
candidates = list(range(bottom, bottom + 16))
random.shuffle(candidates)
print(f'Candidates:\n{candidates}')
best_look = look(candidates, 0.37)
selected = leap(candidates, 0.37, best_look)
print(f'The selected candidate is: {selected}.')
