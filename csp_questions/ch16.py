def grade_average(grades):
    """
      >>> grade_average([100, 40])
      70.0
      >>> grade_average([100, 100, 70])
      90.0
    """
    total = 0
    for score in grades:
        total += score
    return total / len(grades)

def countdown(start):
    """
      >>> countdown(4)
      4
      3
      2
      1
    """
    for i in range(start, 0, -1):
        print(i)


def oddpos(a_list):
    """
      >>> oddpos([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
      [1, 3, 5, 7, 9]
      >>> oddpos(['this', 'that', 'those', 'these'])
      ['that', 'these']
    """
    newlist = []
    for i in range(len(a_list)):
        if i % 2 == 1:
            newlist.append(a_list[i])
    return newlist


def possum(numlist):
    """
      >>> possum([-2, 4, 0, 5])
      9
      >>> possum([2, 4, 6, 5])
      17
    """
    total = 0
    for num in numlist:
        if num > 0:
            total += num
    return total


if __name__ == '__main__':
    import doctest
    doctest.testmod()
