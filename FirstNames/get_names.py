#
# Create a dictionary of 'buckets' (lists) of names starting with each letter
# of the Alphabet using data from https://www.nrscotland.gov.uk/statistics-and-data/statistics/statistics-by-theme/vital-events/births/popular-names/archive/babies-first-names-2007/detailed-tables
#
import csv
import string

names = set();

with open('../NotMyData/babies-first-names-all-names-all-years.csv') as csvf:
    csv_reader = csv.reader(csvf, delimiter=',')
    next(csv_reader)     # skip line with headings

    for row in csv_reader:
        names.add(row[2])

outfile = open('justnames.txt', 'w')

count = 1
for name in names:
    if '-' not in name:
        outfile.write(f'{name}\n')
        count += 1
    if count == 20000:
        break

outfile.close()
"""
names_by_first_letter = {}

for first_letter in string.ascii_uppercase:
    names_by_first_letter[first_letter] = []

for name in names:
    names_by_first_letter[name[0]].append(name)

print(names_by_first_letter)
"""
names_by_first_letter = {}
