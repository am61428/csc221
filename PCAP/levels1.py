class Level0:
    var_0 = 100

    def __init__(self):
        self.var0 = 101

    def fun0(self):
        return 102


class Level1(Level0):
    var_1 = 200

    def __init__(self):
        super().__init__()
        self.var1 = 201

    def fun1(self):
        return 202


class Level2(Level1):
    var_2 = 300

    def __init__(self):
        super().__init__()
        self.var2 = 301

    def fun2(self):
        return 302


thing = Level2()
print(thing.var_0, thing.var0, thing.fun0())
print(thing.var_1, thing.var1, thing.fun1())
print(thing.var_2, thing.var2, thing.fun2())
