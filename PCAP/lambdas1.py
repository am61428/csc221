def print_func_vals(args, f):
    for n in args:
        print(f'f({n}) = {f(n)}')


print_func_vals(range(-2, 3), lambda n: 2 * n ** 2 - 4 * n + 2)
