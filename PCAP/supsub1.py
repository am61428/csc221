class Super:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return 'My name is {}.'.format(self.name)


class Sub(Super):
    def __init__(self, name):
        Super.__init__(self, name)


thing = Sub('Bob')
print(thing)
