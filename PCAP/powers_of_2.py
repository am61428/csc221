def powers_of_2(n):
    power = 1
    for i in range(n):
        yield power
        power *= 2


for val in powers_of_2(14):
    print(val)

p_of_2 = [val for val in powers_of_2(10)]
print(p_of_2)
