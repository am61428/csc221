class SuperA:
    var_a = 10

    def fun_a(self):
        return 11


class SuperB:
    var_b = 20

    def fun_b(self):
        return 21


class Sub(SuperA, SuperB):
    pass


thing = Sub()

print(thing.var_a, thing.fun_a())
print(thing.var_b, thing.fun_b())
