from os import strerror


data = bytearray(10)
try:
    bf = open('data/text.txt', 'rb')
    bf.readinto(data)
    bf.close()
    for b in data:
        print(hex(b), end='')
except IOError as e:
    print('I/O error occurred: ', strerror(e.errno))
