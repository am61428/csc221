def outer(param):
    local_var = param

    def inner():
        return local_var

    return inner


var = 1
func = outer(var)
print(func())
