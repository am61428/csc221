list_comprehension = [1 if n % 2 == 0 else 0 for n in range(10)]
generator = (1 if n % 2 == 0 else 0 for n in range(10))

for val in list_comprehension:
    print(val, end=' ')
print()

for val in generator:
    print(val, end=' ')
print()

try:
    print(len(list_comprehension))
except TypeError:
    print('Can not print a list comprehension length')

try:
    print(len(generator))
except TypeError:
    print('Can not print a generator length')
