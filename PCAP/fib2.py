class Fib:
    def __init__(self, n, debug=False):
        self.debug = debug
        if debug:
            print('Fib __init__')
        self.__n = n
        self.__i = 0
        self.__p1 = self.__p2 = 1

    def __iter__(self):
        if self.debug:
            print('Fib __iter__')
        return self

    def __next__(self):
        self.__i += 1
        if self.__i > self.__n:
            raise StopIteration
        if self.__i in [1, 2]:
            return 1
        ret = self.__p1 + self.__p2
        self.__p1, self.__p2 = self.__p2, ret
        return ret


class Class:
    def __init__(self, n, debug=False):
        self.debug = debug
        if debug:
            print('Class __init__')
        self.__iter = Fib(n, debug)

    def __iter__(self):
        if self.debug:
            print('Class __iter__')
        return self.__iter


thing = Class(8, debug=True)

for i in thing:
    print(i)
