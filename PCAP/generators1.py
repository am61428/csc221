def func(n):
    for i in range(n):
        yield i


for val in func(5):
    print(val)
