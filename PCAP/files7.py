from os import strerror


try:
    bf = open('data/text.txt', 'rb')
    data = bytearray(bf.read())
    bf.close()
    for b in data:
        print(hex(b), end='')
except IOError as e:
    print('I/O error occurred: ', strerror(e.errno))
