class Super:
    sup_var = 1


class Sub(Super):
    sub_var = 2


thing = Sub()
print(thing.sub_var)
print(thing.sup_var)
