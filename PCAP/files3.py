from os import strerror


try:
    stream = open('file.txt', 'rt')
    stream.close()
except Exception as exc:
    print('File could not be opened:', strerror(exc.errno))
