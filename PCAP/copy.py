from os import strerror


srcname = input('Source file name: ')
try:
    src = open(srcname, 'rb')
except IOError as e:
    print('Cannot open source file: ', strerror(e.errno))

dstname = input('Destination file name: ')
try:
    dst = open(dstname, 'wb')
except IOError as e:
    print('Cannot open destination file: ', strerror(e.errno))
    src.close()
    exit(e.errno)

buf = bytearray(65536)
total = 0

try:
    readin = src.readinto(buf)
    while readin > 0:
        written = dst.write(buf[:readin])
        total += written
        readin = src.readinto(buf)
except IOError as e:
    print('Cannot open destination file: ', strerror(e.errno))
    exit(e.errno)

print(total, 'byte(s) successfully written')
src.close()
dst.close()
