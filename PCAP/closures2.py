def make_closure(param):
    local_var = param

    def power(p):
        return p ** local_var

    return power


fsqr = make_closure(2)
fcub = make_closure(3)

print(list(map(fsqr, range(5))))
print(list(map(fcub, range(5))))
