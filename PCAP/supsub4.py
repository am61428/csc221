class Super:
    def __init__(self):
        self.sup_var = 11


class Sub(Super):
    def __init__(self):
        super().__init__()
        self.sub_var = 12


thing = Sub()
print(thing.sub_var)
print(thing.sup_var)
