class Level0:
    var = 100

    def fun(self):
        return 101


class Level1(Level0):
    var = 200

    def fun(self):
        return 201


class Level2(Level1):
    pass


thing = Level2()
print(thing.var, thing.fun())
