def fib(n):
    p1 = p2 = 1
    for i in range(n):
        if i in [0, 1]:
            yield 1
        else:
            n = p1 + p2
            p2, p1 = p1, n
            yield n


fibs = list(fib(10))
print(fibs)
