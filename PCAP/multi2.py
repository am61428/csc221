class Left:
    var = 'L'
    varl = 'LL'

    def fun(self):
        return 'left'


class Right:
    var = 'R'
    varr = 'RR'

    def fun(self):
        return 'right'


class Sub(Left, Right):
    pass


thing = Sub()

print(thing.var, thing.varl, thing.varr, thing.fun())
