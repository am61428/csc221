from turtle import *

space = Screen()
space.setup(800, 800)
alishia = Turtle()
alishia.penup()
alishia.goto(-200, -300)
alishia.setheading(90)
alishia.color('red', 'yellow')
alishia.pensize(5)
alishia.pendown()
alishia.begin_fill()

# Draw a square
for sides in [1, 2, 3, 4]:
    alishia.forward(300 * (1 + (sides % 2)))
    alishia.right(90)

alishia.end_fill()
input()
