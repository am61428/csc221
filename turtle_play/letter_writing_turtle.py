from turtle import *


class LetterWriter(Turtle):
    def __init__(self):
        super().__init__()


    def zero(self):
        for dist, angle in [(50, 90), (100, 90), (50, 90)]:
            self.forward(dist)
            self.left(angle)
        self.forward(100)
        self.goto(self.pos() + (50, 100))
        self.penup()
        self.forward(100)
        self.left(90)
        self.forward(25)
        self.pendown()


bob = LetterWriter()
bob.zero()
