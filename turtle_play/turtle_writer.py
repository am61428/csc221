from turtle import *


def draw_H(a_turtle):
    for a, d in [(90, 100), (180, 50), (90, 50), (90, 50), (180, 100)]:
        a_turtle.left(a)
        a_turtle.forward(d)
    a_turtle.penup()
    a_turtle.left(90)
    a_turtle.forward(25)
    a_turtle.pendown()


def draw_C(a_turtle):
    a_turtle.penup()
    a_turtle.goto(a_turtle.pos() + (50, 100))
    a_turtle.left(180)
    a_turtle.pendown()
    a_turtle.forward(50)
    a_turtle.left(90)
    a_turtle.forward(100)
    a_turtle.left(90)
    a_turtle.forward(50)
    a_turtle.penup()
    a_turtle.forward(25)
    a_turtle.pendown()


def draw_e(a_turtle):
    a_turtle.forward(50)
    a_turtle.left(180)
    for i in range(3):
        a_turtle.forward(50)
        a_turtle.right(90)
    a_turtle.forward(25)
    a_turtle.right(90)
    a_turtle.forward(50)
    a_turtle.penup()
    a_turtle.left(180)
    a_turtle.forward(75)
    a_turtle.right(90)
    a_turtle.forward(25)
    a_turtle.left(90)
    a_turtle.pendown()


def draw_l(a_turtle):
    a_turtle.penup()
    a_turtle.forward(25)
    a_turtle.pendown()
    a_turtle.left(90)
    a_turtle.forward(100)
    a_turtle.left(180)
    a_turtle.penup()
    a_turtle.forward(100)
    a_turtle.left(90)
    a_turtle.forward(50)
    a_turtle.pendown()


def draw_n(a_turtle):
    a_turtle.left(90)
    a_turtle.forward(50)
    a_turtle.right(90)
    a_turtle.forward(50)
    a_turtle.right(90)
    a_turtle.forward(50)
    a_turtle.penup()
    a_turtle.left(90)
    a_turtle.forward(25)
    a_turtle.pendown()


def draw_o(a_turtle):
    for i in range(4):
        a_turtle.forward(50)
        a_turtle.left(90)
    a_turtle.penup()
    a_turtle.forward(75)
    a_turtle.pendown()


def draw_r(a_turtle):
    a_turtle.left(90)
    a_turtle.forward(50)
    a_turtle.right(90)
    a_turtle.forward(50)
    a_turtle.penup()
    a_turtle.forward(25)
    a_turtle.right(90)
    a_turtle.forward(50)
    a_turtle.left(90)
    a_turtle.pendown()


def draw_space(a_turtle):
    a_turtle.penup()
    a_turtle.forward(75)
    a_turtle.pendown()


setup(1200, 300)
connor = Turtle()
connor.penup()
connor.goto(-550, -50)
connor.pendown()
letters = {'H': draw_H, 'e': draw_e, 'l': draw_l, 'o': draw_o, 'C': draw_C,
           'n': draw_n, 'r': draw_r, ' ': draw_space}

message = 'Hello Connor'

for letter in message:
    letters[letter](connor)

input()
